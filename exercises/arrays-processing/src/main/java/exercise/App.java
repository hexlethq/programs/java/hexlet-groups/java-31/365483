package exercise;
import java.util.Arrays;
class App {
    // BEGIN
    static int getIndexOfMaxNegative(int[] arr) {
        int result = -1;
        if (arr.length == 0) {
            return result;
        }
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < 0 && max < arr[i]) {
                max = arr[i];
                result = i;
            }
        }
        return result;
    }

    public static int[] getElementsLessAverage(int[] number) {
        if (number.length == 0) {
            return number;
        }
        int[] number1 = new int[number.length];
        double sum = 0;
        for (int i = 0; i < number.length; i++) {
            sum= sum + number[i];
        }
        double arithmeticMean = sum / number.length;
        int arrLength = 0;
        for (int i = 0, j = 0; i < number.length; i++) {
            if (number[i] < arithmeticMean) {
                number1[j] = number[i];
                j++;
                arrLength = j;
            }
        }
        int[] number2 = Arrays.copyOf(number1, arrLength);
        return number2;
    }
 // END
}

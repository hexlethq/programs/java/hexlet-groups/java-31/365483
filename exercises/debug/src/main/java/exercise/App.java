package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int a, int b, int c) {
        var firstSide = (a == b) && (a != c);
        var secondSide = (b == c) && (b != a);
        var thirdSide = (a == c) && (a != b);
        if (((a + b) <= c) || ((c + a) <= b) || ((b + c) <= a)) {
            return "Треугольник не существует";
        } else if ((a == b) && (a == c)) {
            return "Равносторонний";
        } else if (firstSide || secondSide || thirdSide) {
            return "Равнобедренный";
        } else {
            return "Разносторонний";
    }
            // END
    }
}
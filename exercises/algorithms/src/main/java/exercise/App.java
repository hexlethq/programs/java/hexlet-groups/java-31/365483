package exercise;
import java.util.Arrays;
class App {
    // BEGIN
    public static int[] sort(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int number = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = number;
                }
            }
        }
        return arr;
    }
    public static void main(String[] args) {
        int[] array = {3, 10, 4, 3};
        System.out.println(Arrays.toString(sort(array)));
    }

        // END
}

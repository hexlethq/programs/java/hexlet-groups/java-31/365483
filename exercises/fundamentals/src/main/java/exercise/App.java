package exercise;

class App {
    public static void numbers() {
        // BEGIN
        System.out.println(8 / 2 + 100 % 3);
        // END
    }

    public static void strings() {
        String language = "Java";
        // BEGIN
        String a = " works";
        String b = " on";
        String c = " JVM";
        System.out.println(language + a + b + c);
        // END
    }

    public static void converting() {
        Number soldiersCount = 300;
        String name = "spartans";
        // BEGIN
        System.out.println(soldiersCount + " " + name);
        // END
    }
}

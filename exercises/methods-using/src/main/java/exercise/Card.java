package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
// BEGIN
        String quantityStars = "*".repeat(starsCount);
        String cardNumberX = cardNumber.substring(12, 16);
        System.out.println(quantityStars + cardNumberX);
// END
    }

    public static void main(String[] args) {
        printHiddenCard("1234567812345678", 4);
    }
}
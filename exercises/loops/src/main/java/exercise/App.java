package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String str) {
        var str1 = str.trim();
        var index = 0;
        String abv = "";
        while (index < str1.length()) {
            if (Character.isSpaceChar(str1.charAt(index))) {
                char index1 = str1.charAt(index + 1);
                abv = abv + Character.toUpperCase(index1);
            }
            index++;
        }
        return Character.toUpperCase(str1.charAt(0)) + abv;
    }
    // END
}

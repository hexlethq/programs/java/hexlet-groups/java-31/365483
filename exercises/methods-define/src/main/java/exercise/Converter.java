package exercise;

class Converter {
    // BEGIN
    public static Integer convert(Integer number, String line) {
        if (line.equals("b")) {
           return number * 1024;
       } else if (line.equals("Kb")) {
           return number / 1024;
       }
        return 0;
    }

   public static void main(String[] args) {
       System.out.println("10 Kb = " + convert(10, "b") + " b");
    }

    // END
}
